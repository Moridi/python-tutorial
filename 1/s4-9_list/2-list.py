#list: methods

animals = ["cat", "dog", "zebra", "bear", "penguin", "horse"]


print("animals:")
print(animals)

print("number of elements in list:")
print(len(animals))


animals.append("rhino")
print('"rhino" added: ')
print(animals)
print("number of elements in list:")
print(len(animals))

animals.insert(3, "elephant")
print('"elephant" inserted at 3: ')
print(animals)


animals.remove("horse")
print('"horse" removed: ')
print(animals)


animals.pop()
print('last item popped: ')
print(animals)
