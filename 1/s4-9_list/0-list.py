#list: access, print

snakeList = ["python", "viper", "anaconda", "cobra"]

print("First snake:")
print(snakeList[0])


print("Last snake:")
print(snakeList[-1])

print("3rd snake:")
print(snakeList[2])

print("All snakes:")
print(snakeList)