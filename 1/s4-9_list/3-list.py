#list: copy

snakeList = ["python", "viper", "anaconda", "cobra"]

print("list2 is just a reference not a copy: ")
list2 = snakeList
print("snakelist:", snakeList)
list2.append("kingsnake")
print("list2:", list2)
print("snakelist:", snakeList)


print()

snakeList = ["python", "viper", "anaconda", "cobra"]

print("list3 is a copy: ")
list3 = snakeList.copy()
print("snakelist:", snakeList)
list3.append("kingsnake")
print("list3:", list3)
print("snakelist:", snakeList)