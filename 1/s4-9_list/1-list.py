#list: range indexing

animals = ["cat", "dog", "zebra", "bear", "penguin", "horse", "elephant", "rhino"]


print("animals[3:6] :")
print(animals[3:6])

print("animals[-6:-1] :")
print(animals[-6:-1])

print("animals[::] :")
print(animals[::])
