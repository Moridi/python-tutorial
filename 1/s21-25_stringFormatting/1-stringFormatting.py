#stringFormatting: multiple placeholders


myString = "Hello world! My name is {} and I like {}."

print(myString.format("Sara", "programming"))
print(myString.format("John", "swimming"))
