#stringFormatting: Positional and Keyword Arguments

myString1 = "The quick {0} {2} jumps over the {3} {1}."
print(myString1.format("brown", "dog", "fox", "lazy"))


myString2 = "The quick {color} {animal2} jumps over the {adjective} {animal1}."
print(myString2.format(color="brown", animal1="dog", animal2="fox", adjective="lazy"))