#stringFormatting: Type Specifying

myString1 = "number {:d} in decimal is {:b} in binary and {:x} in hexadecimal."
print(myString1.format(17, 17, 17))



myString2 = "Temperature: {:.4f}"
print(myString2.format(4.123456))
