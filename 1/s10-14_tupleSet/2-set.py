#set: methods

birdSet = {"eagle", "parrot", "owl"}


print("birdSet: ")
print(birdSet)

print('"duck" added to birdSet:')
birdSet.add("duck")
print(birdSet)

print('birdSet updated:')
birdSet.update(["duck", "ostrich", "hummingbird", "vulture"])
print(birdSet)


print('"parrot" removed:')
birdSet.remove('parrot')
print(birdSet)