#tuple: access

languagesTuple = ("python", "R", "C++", "JavaScript", "Java", "PHP")

print("First language:")
print(languagesTuple[0])

print("Last language:")
print(languagesTuple[-1])

print("Second language:")
print(languagesTuple[1])

print("languagesTuple[1:4]:")
print(languagesTuple[1:4])

print("All languages:")
print(languagesTuple)