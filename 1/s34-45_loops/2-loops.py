#loops: reverse a number

number = int(input("Number: "))
reversedNumber = 0
while number > 0:
    remainder = number%10
    number = number//10
    reversedNumber = reversedNumber*10 + remainder

print("Reversed Number = {}".format(reversedNumber))
