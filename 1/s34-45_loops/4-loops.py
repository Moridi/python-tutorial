#while: continue

print("odd numbers:")
i = 0
while i < 50:
    i += 1
    if i%2 == 0:
        continue
    print(i)
    
