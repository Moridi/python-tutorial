#while: break

animals = ["cat", "dog", "zebra", "bear", "penguin", "horse", "elephant", "rhino"]

i = 0
while i < len(animals):
    if animals[i] == "horse":
        print("'horse' found at {}".format(i))
        break
    i += 1
