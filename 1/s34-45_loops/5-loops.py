#for: basic

animals = ["cat", "dog", "zebra", "bear", "penguin", "horse", "elephant", "rhino"]

for animal in animals:
    print(animal)
