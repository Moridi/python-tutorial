#for: is number prime?

number = int(input("positive integer number: "))
isPrime = True

for i in range(2, number//2):  
    if number%i == 0: 
        isPrime = False
        break

if number == 1:
    print("not prime.")
elif isPrime:
    print("prime.")
else:
    print("not prime.")