
#loops: infinite

while True:
    print("For + press 1\nFor - press 2\n"
          "For * press 3\nFor Exit press 0")

    pressedKey = int(input())

    if pressedKey == 0:
        break
    elif pressedKey == 1:
        a = int(input())
        b = int(input())
        print(str(a) + " + " + str(b) + " = " + str(a+b))
    elif pressedKey == 2:
        a = int(input())
        b = int(input())
        print(str(a) + " - " + str(b) + " = " + str(a - b))
    elif pressedKey == 3:
        a = int(input())
        b = int(input())
        print(str(a) + " * " + str(b) + " = " + str(a * b))
    else:
        print("Something went wrong, try again!")

print("Thanks for using this program.")
