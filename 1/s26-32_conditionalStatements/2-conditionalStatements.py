#condition: else-elif-if

print("How old are you? ")
age = int(input())

if age < 0:
    print("You are unborn!")
elif age < 3:
    print("Hi little baby!")
elif age < 13:
    print("Still a kid!")
elif age < 19:
    print("Growing up fast!")
else:
    print("You are an adult!")
