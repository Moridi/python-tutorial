#dictionary: methods

human = {"name": "John", "last name": "Doe", "age": 25}

print("age: ")
print(human["age"])

print("gpa addad: ")
human["gpa"] = 16.75
print(human)

print("gpa changed:")
human["gpa"] = 17.5
print(human)

print("values:")
print(human.values())


print("checking: is 'name' in dictionary?")
print("name" in human)