#dictionary: copy

human = {"name": "John", "last name": "Doe", "age": 25}

print("human2 is just a reference not a copy: ")
human2 = human
print("human:", human)
human2["gpa"] = 17.5
print("human2:", human2)
print("human:", human)


print()

human = {"name": "John", "last name": "Doe", "age": 25}

print("human3 is a copy: ")
human3 = human.copy()
print("human:", human)
human3["gpa"] = 17.5
print("human3:", human3)
print("human:", human)