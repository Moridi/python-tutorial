#dictionary: remove

human = {"name": "John", "last name": "Doe", "age": 25}

print('"age" removed: ')
human.pop("age")
print(human)