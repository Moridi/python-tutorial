######## Creating a String ########

#1. Creating a String with single Quotes 
String1 = 'Welcome to the Geeks World'
print("String with the use of Single Quotes: ") 
print(String1) 
  
#2. Creating a String with double Quotes 
String1 = "I'm a superhero"
print("\nString with the use of Double Quotes: ") 
print(String1) 
  
#3. Creating a String with triple Quotes 
String1 = '''I'm a superhero and I live in a world of "DC comics"'''
print("\nString with the use of Triple Quotes: ") 
print(String1) 
  
# Creating String with triple Quotes allows multiple lines 
String1 = '''hero 
            For 
            Life'''
print("\nCreating a multiline String: ") 
print(String1) 



######## Accessing characters ########
String1 = "Comic Freak"
print("Initial String: ") 
print(String1) 
  
# Printing First character 
print("\nFirst character of String is: ") 
print(String1[0]) 
  
# Printing Last character 
print("\nLast character of String is: ") 
print(String1[-1])



######## slicing ########
# Creating a String 
String1 = "Marvel Fan"
print("Initial String: ")  
print(String1) 
  
# Printing 3rd to 12th character 
print("\nSlicing characters from 3-12: ") 
print(String1[3:12]) 
  
# Printing characters between  
# 3rd and 2nd last character 
print("\nSlicing characters between " +
    "3rd and 2nd last character: ") 
print(String1[3:-2])




######## Deleting/Updating ########
String1 = "Hello, I'm Peter Parker"
print("Initial String: ") 
print(String1) 
  
# 1.Updating a character  
# of the String 
String1[2] = 'p' # ERROR
print("\nUpdating character at 2nd Index: ") 
print(String1)

# 2.Updating a String 
String1 = "I miss MJ:("
print("\nUpdated String: ") 
print(String1)

#3. Deleting a character  
# of the String 
del String1[2] # will ERROR  
print("\nDeleting character at 2nd Index: ") 
print(String1)

#4. Deleting a String 
# with the use of del 
del String1  
print("\nDeleting entire String: ") 
print(String1)# will ERROR



######## Escape Sequencing ########
# Initial String 
String1 = '''I'm a "captain america"'''
print("Initial String with use of Triple Quotes: ") 
print(String1) 
  
# Escaping Single Quote  
String1 = 'I\'m a "CA"'
print("\nEscaping Single Quote: ") 
print(String1) 
  
# Escaping Doule Quotes 
String1 = "I'm a \"CA\""
print("\nEscaping Double Quotes: ") 
print(String1) 
  
# Printing Paths with the  
# use of Escape Sequences 
String1 = "C:\\Python\\CA\\"
print("\nEscaping Backslashes: ") 
print(String1)