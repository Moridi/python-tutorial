#####################
## Creating variables
#####################
#1. 
x = 5
y = "John"
print(x)
print(y)

#2.
x = 4 # x is of type int
x = "Sally" # x is now of type str
print(x)

#3.
x = "John"
# is the same as
x = 'John'


#####################
## variable names
#####################
#Legal variable names:
myvar = "John"
my_var = "John"
_my_var = "John"
myVar = "John"
MYVAR = "John"
myvar2 = "John"

#Illegal variable names:
2myvar = "John"
my-var = "John"
my var = "John"


#####################
## Assign values to multiple variables
#####################
#1.
x, y, z = "Orange", "Banana", "Cherry"
print(x)
print(y)
print(z)

#2.
x = y = z = "Orange"
print(x)
print(y)
print(z)


#####################
## Output Variables
#####################
#1.
x = "awesome"
print("Python is " + x)

#2.
x = "Python is "
y = "awesome"
z =  x + y
print(z)

#3.
x = 5
y = 10
print(x + y)