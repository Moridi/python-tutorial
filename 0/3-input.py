# raw_input ( ) : 
# This function works in older version (like Python 2.x).
# This function takes exactly what is typed from the keyboard,
#  convert it to string and then return it to the variable in which we want to store. 
# For example –
g = raw_input("Enter your name : ") 
print(g)


# input ( ) :
#  This function first takes the input from the user and then evaluates the expression,
#  which means Python automatically identifies whether user entered a string or a number or list.
#  If the input provided is not correct then either syntax error or exception is raised by python.
#  For example –
val = input("Enter your value: ") 
print(val)



#another example  
num = input ("Enter number :") 
print(num) 
name1 = input("Enter name : ") 
print(name1) 
  
# Printing type of input value 
print ("type of number", type(num)) 
print ("type of name", type(name1)) 