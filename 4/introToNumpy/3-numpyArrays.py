#numpy: dtype array
import numpy as np

myList = [[1, 2, 3, 4], [5, 6, 7, 8]]

arr = np.array(myList, dtype=str)
print(arr)