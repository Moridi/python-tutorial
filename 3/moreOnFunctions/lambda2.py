####################
# lambda example 
####################

myList = {3, 10 , 6, 5, 36, 90, 1, 2}
###################################
# you can use lambda as a filter
####################################
result = list(filter(lambda x: (x%6 == 0), myList))
print(result)


myList = {"aa", "assa", "aoeu", "madam", "hello", 'bird'}
result = list(filter(lambda x: (x == "".join(reversed(x))), myList))
#it will keep palindrome strings
print(result)