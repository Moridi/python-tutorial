####################
# lambda example 
####################

imdbRating = [
        {"name": "Parasite", "rating": 8.6},
        {"name": "Thor", "rating": 7.0},
        {"name": "Joker", "rating": 8.5},
        {"name": "The Dark Knight", "rating": 9.0},
        {"name": "Finding Nemo", "rating": 8.1}
    ]


###################################
# you can use lambda as sorting key
####################################
imdbRating.sort(key=lambda movie: movie["rating"], reverse=True) 
# list is sorted based on movie ratings
# reverse=True means high to low
for movie in imdbRating:
    print("{} : {}".format(movie["name"], movie["rating"]))