###########################
# *args, **kwargs example
###########################

def sumAll(*numbers):
    result = 0
    for num in numbers:
        result += num
    return result

def printInfo(**information):
    print("information:")
    for data in information:
        print("{}: {}".format(data, information[data]))

print("sum= {}".format(sumAll(3, 5, 7, 2)))


printInfo(name="John", age="25", job="Programmer")