USER_ROLE_ADMIN = "admin"
USER_ROLE_CUSTOMER = "customer"

class User:
    def __init__(self, userDId):
        self.id = userDId
        self.cart = []
        self.totalCost = 0

    def registerUser(self, name, familyName, email, role):
        self.name = name
        self.familyName = familyName
        self.email = email
        self.role = role
    
    def buy(self, chosenGood):
        self.cart.append(chosenGood)
        self.totalCost += chosenGood.getPrice()
    
    def printCart(self):
        print("{} {}'s cart".format(self.name, self.familyName))
        for cartGood in self.cart:
            print("item: {}\t{}".format(cartGood.getName(), cartGood.getPrice()))
        print("Total cost: {} toman".format(self.totalCost))
