###################
# Package example
###################

# shop is a package
# it has 2 modules: good, user 
import shop.good as good
from shop import user as user

good1 = good.Good("Last of us", 450000)
good2 = good.Good("God of war", 400000)

good3 = good.Good("Spider-man", 350000)
good4 = good.Good("Uncharted4", 250000)

customer1 = user.User(1)
customer1.registerUser("John", "Doe", "john@doe.com", user.USER_ROLE_CUSTOMER)

customer2 = user.User(2)
customer2.registerUser("Sara", "Smith", "Sara@Smith.com", user.USER_ROLE_CUSTOMER)

customer1.buy(good1)
customer1.buy(good2)
customer1.printCart()
print()
customer2.buy(good3)
customer2.buy(good4)
customer2.printCart()