#######################################
# from moduleName import * (everything)
#######################################

from Player import *

p1 = Doctor("Behzad", PLAYER_ROLE_DOCTOR)
p2 = Mafia("Sajjad", PLAYER_ROLE_MAFIA)
p3 = Vilager("Ghazal", PLAYER_ROLE_VILAGER)
p4 = Detective("Mohammad", PLAYER_ROLE_DETECTIVE)

p1.stateRole()
p2.voteToKill(p3)
p1.heal(p2)
p4.detect(p1)