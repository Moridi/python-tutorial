############################
# Module example
# We are going to add it to
# other scripts as a module
############################

PLAYER_ROLE_DETECTIVE = "detective"
PLAYER_ROLE_VILAGER = "vilager"
PLAYER_ROLE_DOCTOR = "doctor"
PLAYER_ROLE_MAFIA = "mafia"

class Player:
    def __init__(self, name, role):
        self.name = name
        self.role = role
    
    def stateRole(self):
        print("I am {}".format(self.role))
    
    def getName(self):
        return self.name
    
class Mafia(Player):
    def voteToKill(self, player):
        print("I kill {}".format(player.getName()))

class Doctor(Player):
    def heal(self, player):
        print("I heal {}".format(player.getName()))

class Vilager(Player):
    pass

class Detective(Player):
    def detect(self, player):
        print("What is {}'s role?".format(player.getName()))