##################################
# import moduleName as name
##################################

import Player as pl

p1 = pl.Doctor("Mohammad", pl.PLAYER_ROLE_DOCTOR)
p2 = pl.Mafia("Ghazal", pl.PLAYER_ROLE_MAFIA)
p3 = pl.Vilager("Sajjad", pl.PLAYER_ROLE_VILAGER)
p4 = pl.Detective("Behzad", pl.PLAYER_ROLE_DETECTIVE)

p1.stateRole()
p2.voteToKill(p4)
p1.heal(p3)
p4.detect(p1)