##################################
# from moduleName import something
##################################


from Player import Doctor, Mafia, PLAYER_ROLE_DOCTOR, PLAYER_ROLE_MAFIA

p1 = Doctor("Mohammad", PLAYER_ROLE_DOCTOR)
p2 = Mafia("Sajjad", PLAYER_ROLE_MAFIA)

p1.stateRole()
p2.voteToKill(p1)
