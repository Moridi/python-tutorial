#####################
# import moduleName
#####################

import Player

p1 = Player.Doctor("Mohammad", Player.PLAYER_ROLE_DOCTOR)
p2 = Player.Mafia("Ghazal", Player.PLAYER_ROLE_MAFIA)
p3 = Player.Vilager("Sajjad", Player.PLAYER_ROLE_VILAGER)
p4 = Player.Detective("Behzad", Player.PLAYER_ROLE_DETECTIVE)

p1.stateRole()
p2.voteToKill(p3)
p1.heal(p3)
p4.detect(p2)